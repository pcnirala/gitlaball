COMMAND LINE INSTRUCTIONS
<br>
Git global setup
<br>
git config --global user.name "Prakash Nirala "
<br>
git config --global user.email "pcnirala@gmail.com"
<br>

CREATE A NEW REPOSITORY
<br>
git clone https://gitlab.com/pcnirala/gitlaball.git
<br>
cd gitlaball
<br>
touch README.md
<br>
git add README.md
<br>
git commit -m "add README"
<br>
git push -u origin master
<br>

EXISTING FOLDER
<br>
cd existing_folder
<br>
git init
<br>
git remote add origin https://gitlab.com/pcnirala/gitlaball.git
<br>
git add .
<br>
git commit -m "Initial commit"
<br>
git push -u origin master
<br>

EXISTING GIT REPOSITORY
<br>
cd existing_repo
<br>
git remote rename origin old-origin
<br>
git remote add origin https://gitlab.com/pcnirala/gitlaball.git
<br>
git push -u origin --all
<br>
git push -u origin --tags