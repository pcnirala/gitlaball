<p>The development server here is the webpack development server, which is a tool to run your application with live reloading.&nbsp;</p>
<p>Please follow the below steps</p>
<ol>
<li>To start off, you would need to install Node.js (version 8 is recommended) and Yarn (the dependency manager, <a href="https://github.com/yarnpkg/yarn/releases/download/v0.21.3/yarn-0.21.3.msi">https://github.com/yarnpkg/yarn/releases/download/v0.21.3/yarn-0.21.3.msi</a>).</li>
<li>In the command line, run `yarn` to install all the dependencies (webpack-dev-server is one of them)</li>
<li>Run `yarn start` which will in turn start the webpack development server&nbsp;</li>
</ol>
<p><strong>For those who completed the course</strong>&nbsp;</p>
<ol>
<li>Start the development server, the app will be running at <a href="http://localhost:8081/">http://localhost:8081/</a>.&nbsp;</li>
</ol>
<ol start="2">
<li>There is an error, use the command line or Chrome DevTools to identify and fix it.&nbsp;</li>
</ol>
<ol start="3">
<li>The page now displays a cryptocurrency, its price and the percentage change in the last 24 hours.&nbsp;</li>
</ol>
<ol start="4">
<li>The objective is to display the data of the top 5 cryptocurrencies in the market. Call `getCryptoData` function in `src/api/api.js` to fetch real time data and display the full list in a similar design.&nbsp;</li>
</ol>
<ol start="5">
<li>Add a select/dropdown element to the page, with the following currency options "SGD", "AUD", "EUR", "GBP", "USD", "VND". Changing the value of this dropdown should change the display currency of the cryptocurrencies price. Hint: `getCryptoData` function accepts a `displayCcy` argument and will return the price data in the specified currency.&nbsp;</li>
</ol>
<ol start="6">
<li>Improve the UI using CSS. For example, change the percentage box background to green if the value is positive and to red if negative.&nbsp;</li>
</ol>
<ol start="7">
<li>[Advanced] If your CrytoCcyList component is stateful, rewrite using Redux to make it stateless.</li>
</ol>
<p><strong>UI (React) online course training</strong></p>
<p>Target audience</p>
<p>People willing to take online course to learn UI programming (React, Typescript) to extend their knowledge and skills&nbsp;</p>
<p>Objective</p>
<p>Get familiar with SKY UI tech stack to be able to work on UI-related tasks&nbsp;</p>
<p>The aim of the email is to share links to available online courses / materials, people who are interested in the subject to watch and complete course/s and get assessed for what they learnt&nbsp;</p>
<p>Please don&rsquo;t hesitate to contact me in case of any questions related to the subject, also suggestions are very welcome&nbsp;</p>
<p>Timeline</p>
<p>We expect below online course/s to be completed by Wednesday 14<sup>th</sup> Mar EOD. Assessment will be conducted starting Thursday and upon confirmation the course is completed. It will be a short test program to help wrap up the material you received whilst watching the online course.&nbsp;</p>
<p><strong>Course</strong>&nbsp;</p>
<p>Either one</p>
<ul>
<li><a href="https://egghead.io/courses/start-learning-react">https://egghead.io/courses/start-learning-react</a></li>
<li><a href="https://www.codecademy.com/learn/react-101">https://www.codecademy.com/learn/react-101</a>&nbsp;</li>
</ul>
<p>Advanced Topics</p>
<ul>
<li>Redux I: <a href="https://egghead.io/courses/getting-started-with-redux">https://egghead.io/courses/getting-started-with-redux</a></li>
<li>Redux II: <a href="https://egghead.io/courses/building-react-applications-with-idiomatic-redux">https://egghead.io/courses/building-react-applications-with-idiomatic-redux</a>&nbsp;</li>
</ul>
<p>Useful Links&nbsp;</p>
<ul>
<li><strong>ReactJS</strong>: <a href="https://reactjs.org/tutorial/tutorial.html">https://reactjs.org/tutorial/tutorial.html</a></li>
<li>Typescript: <a href="http://www.typescriptlang.org/docs/home.html">http://www.typescriptlang.org/docs/home.html</a></li>
<li>Typescript React Starter: <a href="https://github.com/Microsoft/TypeScript-React-Starter#typescript-react-starter">https://github.com/Microsoft/TypeScript-React-Starter#typescript-react-starter</a></li>
<li>Redux Saga: <a href="https://redux-saga.js.org/docs/introduction/BeginnerTutorial.html">https://redux-saga.js.org/docs/introduction/BeginnerTutorial.html</a></li>
<li>Testing with Jest and Enzyme: <a href="https://hackernoon.com/testing-react-components-with-jest-and-enzyme-41d592c174f">https://hackernoon.com/testing-react-components-with-jest-and-enzyme-41d592c174f</a></li>
</ul>